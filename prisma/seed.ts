import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()


async function main() {
  const user = await prisma.user.upsert({
    where: { email: "test@test.com" },
    update: {},
    create: {
      email: "test@test.com",
      name: "Juanito",
      password: "1234"
    }
  })

  console.log({ user })
}

main()
  .then(() => prisma.$disconnect())
  .catch(async (e) => {
    console.error("Error: " + e)
    await prisma.$disconnect()
    process.exit(1)
  })
